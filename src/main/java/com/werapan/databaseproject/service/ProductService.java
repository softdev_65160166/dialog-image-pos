/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
    public Product getByTel(String tel) {
        ProductDao ProductDao = new ProductDao();
        Product Product = ProductDao.getByTel(tel);
        return Product;
    }
    
    public List<Product> getProducts(){
        ProductDao ProductDao = new ProductDao();
        return ProductDao.getAll(" Product_Id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao ProductDao = new ProductDao();
        return ProductDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao ProductDao = new ProductDao();
        return ProductDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao ProductDao = new ProductDao();
        return ProductDao.delete(editedProduct);
    }

    
    
}
